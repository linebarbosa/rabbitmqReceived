FROM fedora:28
RUN dnf install -y java-1.8.0-openjdk
RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-6.2.4-x86_64.rpm
RUN rpm -vi filebeat-6.2.4-x86_64.rpm
RUN dnf install -y initscripts
COPY ./rabbitmqReceived/filebeat.yml /etc/filebeat/filebeat.yml
COPY ./rabbitmqReceived/start.sh /usr/local/bin/start.sh
COPY ./rabbitmqReceived/target/received-0.0.1-SNAPSHOT.jar /app.jar
RUN chmod +x /usr/local/bin/start.sh
CMD /usr/local/bin/start.sh
